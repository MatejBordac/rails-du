class PostsController < ApplicationController
  def index
    @posts = Post.order(updated_at: :desc)
    # @tags = Tag.joins(:posts).group("posts_tags.tag_id").order("COUNT(tags.id) DESC")
    # @tags = Tag.all
    @tags = Tag.joins(:posts).select('tags.*, count(tag_id) as "tag_count"').group('tags.id').order('tag_count desc')
  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find(params[:id])
  end

  def filter
    tag = Tag.find_by_name(params[:name])
    @posts = Post.joins(:posts_tags).where(posts_tags: { tag_id: tag.id }).order(updated_at: :desc)
    # @tags = Tag.joins(:posts_tags).select('tag_id, count(tag_id) as "tag_count"').group(:tag_id).order(' tag_count desc')
    # @tags = Tag.joins(:posts).group("posts_tags.tag_id").order("COUNT(tags.id) DESC")
    @tags = Tag.joins(:posts).select('tags.*, count(tag_id) as "tag_count"').group('tags.id').order('tag_count desc')
    # @tags = Tag.all
    render :index
  end

  def show
  end

  def create
    @post = Post.new(post_params)
    tag_names = params['post']['tags_string'].gsub(',', ' ').split(' ')
    tag_names.each do |tag_name|
      tag = Tag.find_by_name(tag_name)
      tag = Tag.create(name: tag_name) unless tag
      @post.tags.push tag
    end

    if @post.valid?
      @post.save
      redirect_to root_path
    else
      render :new
    end
  end

  def update
    @post = Post.find(params[:id])
    @post.update_attributes(post_params)
    @post.tags.clear
    tag_names = params['post']['tags_string'].gsub(',', ' ').split(' ')
    tag_names.each do |tag_name|
      tag = Tag.find_by_name(tag_name)
      tag = Tag.create(name: tag_name) unless tag
      @post.tags.push tag
    end

    if @post.valid?
      @post.save
      redirect_to root_path
    else
      render :edit
    end
  end

  def destroy
    post = Post.find(params['id'])
    post.destroy if post
    delete_free_tags
    redirect_to root_path
  end

  def post_params
    params.require(:post).permit(:author, :title, :body)
  end

  def delete_free_tags
    Tag.all.each do |tag|
      tag.destroy if tag.posts.empty?
    end
  end
end
