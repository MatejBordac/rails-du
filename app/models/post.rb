class Post < ActiveRecord::Base
  has_and_belongs_to_many :tags
  validates :author, :title, :body, presence: true
  validates :tags, length: { minimum: 1, message: 'must have at least one tag' }
end
