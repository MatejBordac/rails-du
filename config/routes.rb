Rails.application.routes.draw do

  root 'posts#index'
  get 'posts/filter/:name' => 'posts#filter', as: :filtered_posts
  resources :posts

end
